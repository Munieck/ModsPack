@echo off

for %%a in (.) do set currentfolder=%%~na

cd "..\SpaceEngineers\Bin64"

".\SEWorkshopTool.exe" --upload --dry-run --compile --mods "%appdata%\SpaceEngineers\Mods\%currentfolder%" --exclude .csproj .sln .user .gitignore .bat --tags block

echo verify finished

timeout 60