#region pre_script
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using Sandbox.Common;
//using Sandbox.Common.Components;
//using Sandbox.Common.ObjectBuilders;
using Sandbox.Definitions;
using Sandbox.Engine;
using Sandbox.ModAPI;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using Sandbox.Game;
using VRage.ObjectBuilders;
using VRage.Game;
//using VRage.Components;

namespace Commands
    {
        [Sandbox.Common.MySessionComponentDescriptor(Sandbox.Common.MyUpdateOrder.AfterSimulation)]
        public class GPSUtilityCommands : MySessionComponentBase
        {
        #endregion pre_script

        public bool init = false;

        public bool doRange = false;
        public double range = 0.0;

        public void Init()
        {
            if (init == false)
            {
                MyAPIGateway.Utilities.ShowMessage("Rebels-Games.com", "Servers ALPHA, BETA, GAMMA, DELTA");
            }
            init = true;

            MyAPIGateway.Utilities.MessageEntered += ProcessMessage;
        }

        protected override void UnloadData()
        {
            init = false;

            MyAPIGateway.Utilities.MessageEntered -= ProcessMessage;

            base.UnloadData();
        }

        public override void UpdateAfterSimulation()
        {
            if (!init)
            {
                if (MyAPIGateway.Session == null)
                    return;
                Init();
            }

            if (doRange)
            {
                long playerID = MyAPIGateway.Session.Player.PlayerID;
                List<VRage.Game.ModAPI.IMyGps> gps = MyAPIGateway.Session.GPS.GetGpsList(playerID);
                foreach (VRage.Game.ModAPI.IMyGps g in gps)
                {
                    VRageMath.Vector3D playerPos = MyAPIGateway.Session.Player.GetPosition();

                    double distance = VRageMath.Vector3D.Distance(playerPos, g.Coords);

                    if (distance > range)
                    {
                        MyAPIGateway.Session.GPS.SetShowOnHud(playerID, g.GetHashCode(), false);
                    }
                    else
                    {
                        MyAPIGateway.Session.GPS.SetShowOnHud(playerID, g.GetHashCode(), true);
                    }
                }
            }
        }

        public void WriteMessage(string message)
        {
            MyAPIGateway.Utilities.ShowMessage("Info", message);
            MyAPIGateway.Utilities.ShowNotification(message, 2000, MyFontEnum.Blue);
        }

        // gps help
        // gps hide|h <tag>
        // gps show|s <tag>
        // gps hideall|ha
        // gps showall|sa
        // gps showonly|so <tag>
        // gps cardinals
        // gps add|a [name] [sensor]
        // gps appendore|ao
        //
        // TDD:
        //  add ore data from sensors and appendore
        //  Add cardinals for planets
        //  filter beacons/antennas
        void ProcessMessage( string messageText, ref bool echo)
        {
            if (!messageText.StartsWith("/")) { return; }
            List <string> words = new List<string> (messageText.Trim().Replace("/", "").Split(' '));

            if(words.Count >= 1)
            {
                gpsProcess(words);
                log(messageText);   // testing
                echo = false;
            }
        }

        void gpsProcess(List<string> words )
        {
            switch (words[0])
            {
                case "donate":
                    string helpMessage =   " Credits in the game \n" +
                                            "\n" +
                                            "                   CREDITS \n" +
                                            "              1 € -   40.000 c.\n" +
                                            "             2 € -   80.000 c.\n" +
                                            "             5 € -  200.000 c.\n" +
                                            "            10 € -  400.000 c. +   400.000 c. for FREE! \n" +
                                            "            15 € -  600.000 c. +   900.000 c. for FREE! \n" +
                                            "            20 € -  800.000 c. +  2200.000 c. for FREE! \n" +
                                            "            30 € - 1200.000 c. +  3300.000 c. for FREE! \n" +
                                            "            45 € - 1600.000 c. +  4600.000 c. for FREE! \n" +
                                            "            60 € - 2400.000 c. + 7600.000 c. for FREE! \n" +
                                            "\n" +
                                            "              SUPER CREDITS \n" +
                                            "              1 € - 125 sc.\n" +
                                            "             2 € - 250 sc.\n" +
                                            "             5 € - 800 sc.\n" +
                                            "            10 € - 2000 sc. +       500 sc. for FREE! \n" +
                                            "            15 € - 3000 sc. +      1500 sc. for FREE! \n" +
                                            "            20 € - 4000 sc. +     3000 sc. for FREE! \n" +
                                            "            30 € - 6000 sc. +     4000 sc. for FREE! \n" +
                                            "            45 € - 9000 sc. +     9000 sc. for FREE! \n" +
                                            "            60 € - 12000 sc.+   38000 sc. for FREE! \n" +
                                            "\n" +
                                            "               The shop is at: www.rebels-games.com/shop\n" +
                                            "\n" +
                                            "               Enter you nick name and grid name in which you \n" +
                                            "               want the credits to appear\n" +
                                            "               Example: Nick - (Nickname of ship)\n" +
                                            "              (Realization time 24h)\n" +
                                            "\n" +
                                            "              Adds coordinates of station (Command: /gps)\n" +
                                            "\n" +
                                            "                (All funds allocated for server maintenance)";

                    MyAPIGateway.Utilities.ShowMissionScreen("Donate", "paypal.me/lostspace", " ", helpMessage, null, "OK");

                    break;

                case "discord":
                    string helpMessage2 =   "\n" + "\n" + "\n" + "\n" + "\n" + "\n" + "\n" + "\n" + "\n" +
                                            "                                         Discord  Server Invite Link \n" +
                                            "\n" + "                                               discord.gg/KWmrpey \n";

                    MyAPIGateway.Utilities.ShowMissionScreen("Discord", "Voice Communicator", " ", helpMessage2, null, "OK");

                    break;

                case "joinhelp":
                    string helpMessage3 =
                        "                   To jump on another server and move your ship\n" +
                        "\n" +
                        " Use command chat:\n" +
                        " /join 1 - Alpha\n" +
                        " /join 2 - Beta\n" +
                        " /join 3 - Gamma\n" +
                        " /join 4 - Delta\n" +
                        " /join 5 - [Work in Progress]\n" +
                        "\n" +
                        " /joinstatus - Shows jump status.\n" +
                        "\n" +
                        "Max 1500 blocks.\n" +
                        "Be sure to have a medical room\n" +
                        "After the jump choose 'Space suit' to not lose the ship.\n" +
                        "\n" +
                        "If you select 'Space Suit' for the second time, will lose access to you objects.\n" +
                        "So be aware! \n" +
                        "\n" +
                        "                     (Remember! You're jumping on your own risk!)\n" +
                        "              All objects lost during server jump will not be restored!";

                    MyAPIGateway.Utilities.ShowMissionScreen("Join Server", "Rebels-Games.com", " ", helpMessage3, null, "OK");

                    break;

                case "help":
                    string helpMessage4 =   "\n" + 
                                            "                                             Administrator: TimPL \n" + 
                                            "\n" + 
                                            "Simple commands: \n" + 
                                            "!grids list    - Find your objects \n" + 
                                            "/help          - This text \n" + 
                                            "/joinhelp    - Jump on another server \n" + 
                                            "/sectors    - Seizing sectors \n" + 
                                            "/oreinfo     - Ore destiny \n" + 
                                            "/discord    - Discord invite link \n" + 
                                            "/donate     - How to donate for this project - Information about credits \n" + 
                                            "/motd        - Description and server rules \n" + 
                                            "/gps          - Adds all coordinates of station/monoliths \n" + 
                                            "\n" + 
                                            "\n" + 
                                            "                           Website \n" + 
                                            "                  www.rebels-games.com \n" + 
                                            "\n" +
                                            "                           Donate                                                  Discord \n" + 
                                            "            www.rebels-games.com/donate              discord.gg/KWmrpey \n";

                    MyAPIGateway.Utilities.ShowMissionScreen("HELP", "commands", " ", helpMessage4, null, "OK");

                    break;
                case "oreinfo":
                    string helpMessage5 =    "\n" +
                                            "              Asteroids Destiny Ore:                      Planets Destiny Ore: \n" +
                                            "              + Cobalt:            Yes                          + Cobalt:            Yes \n" +
                                            "              + Gold:               Yes                          + Gold:               Yes \n" +
                                            "              + Iron:                Yes                          + Iron:                Yes \n" +
                                            "              + Lateryt:           No                           + Lateryt:           Yes \n" +
                                            "              - Magnesium:    No                            - Magnesium:    Yes \n" +
                                            "              + Malachit:        Yes                          + Malachit:         Yes \n" +
                                            "              - Nickel:             No                           + Nickel:             Yes \n" +
                                            "              + Palladium:      Yes                          - Palladium:       No \n" +
                                            "              - Platinum:         No                           + Platinum:         Yes \n" +
                                            "              - Silicon:            No                           + Silicon:            Yes \n" +
                                            "              + Silver:             Yes                         + Silver:              Yes \n" +
                                            "              + Stone:             Yes                         + Stone:              Yes \n" +
                                            "              - Uranium:          No                           + Uranium:         Yes \n" +
                                            "\n" +
                                            "                                          Alpha Server - Dead Sun Ore \n" +
                                            "                                         Unknown Material    ->     Cez \n" +
                                            "                                         Unknown Material    ->     Rad \n" +
                                            "                                         Unknown Material    ->     Kaliforn \n" +
                                            "                                         Unknown Material    ->     Diamond \n" +
                                            "\n" +
                                            "                                                           Refining \n" +
                                            "                                         Custom Ore        to      Ingot \n" +
                                            "                                         Lateryt                ->     Aluminium \n" +
                                            "                                         Malachit             ->     Copper \n" +
                                            "                                         Palladium Ore    ->     Palladium \n" +
                                            "\n" +
                                            "                                                      IN Refinery: \n" +
                                            "                                                      - All Ore \n" +
                                            "                                                      - Scrap Metal \n" +
                                            "\n" +
                                            "                                                  IN Arc Furnace: \n" +
                                            "                                                  - Cobalt Ore \n" +
                                            "                                                  - Iron Ore \n" +
                                            "                                                  - Nickel Ore \n" +
                                            "                                                  - Scrap Metal \n" + "\n";

                    MyAPIGateway.Utilities.ShowMissionScreen("Ore Density", "Rebels-Games.com", " ", helpMessage5, null, "OK");

                    break;
                case "sectors":
                    string helpMessage6 =   "Works are in progress\n" +
                                            "\n" +
                                            "Fraction can take any sector in the world game.\n" +
                                            "Determinant of each sector is a planet.\n" +
                                            "To occupy the sector has been approved\n" +
                                            "Just inform the administrator\n" +
                                            "And take over Monolith\n" +
                                            "Sector - 250km from the planet.\n" +
                                            "LCD Block was equips the map sectors.\n" +
                                            "\n" +
                                            "Monolith (/gps - Adds all coordinates)\n" +
                                            "\n" +
                                            "                              Warning\n" +
                                            "The acquisition of a monolith without representative faction\n" +
                                            "                  will be severely punished";

                    MyAPIGateway.Utilities.ShowMissionScreen("Sectors", "Rebels-Games.com", " ", helpMessage6, null, "OK");

                    break;

                case "motd":
                    string helpMessage7 =   "Hello Space Engineers! \n" +
                                            "\n" +
                                            "Welcome to REBELS-GAMES universe \n" +
                                            " \n" +
                                            "ALL INFO ABOUT SERVERS ( restarts, maintenance breaks and other changes on servers, what U should know not to loose your stuff ) \n" +
                                            "IN REBELS-GAMES DISCORD CHANEL: \n" +
                                            "\n" +
                                            "https://discord.gg/KWmrpey \n" +
                                            "#announcements TAG \n" +
                                            "\n" +
                                            "U can come back to this screen by typing this in the chat -> /motd \n" +
                                            " \n" +
                                            "Scroll down for: \n" +
                                            "1. Simple commands \n" +
                                            "2. Links \n" +
                                            "3. How server jump works \n" +
                                            "4. Rules in REBELS-GAMES universe \n" +
                                            "5. Modded blocks ( MK2-5 ADVANCED REBEL CANNONS ) \n" +
                                            "6. Limits for blocks on servers \n" +
                                            "____________________________________________________________________ \n" +
                                            " \n" +
                                            "1. SIMPLE COMMANDS \n" +
                                            " \n" +
                                            "/motd        -> U ARE HERE ;) \n" +
                                            "/help        -> All commands \n" +
                                            "/joinhelp    -> Server jump info ( description below ) \n" +
                                            "/sectors     -> How to claim a sector \n" +
                                            "/oreinfo     -> Which ore are on planets and wich are in space \n" +
                                            "/discord     -> Discord server link \n" +
                                            "/shop        -> Price for Credits and SuperCredits \n" +
                                            "/donate      -> Bank account and paypal link \n" +
                                            "/gps         -> Adds planet coordinates to GPS \n" +
                                            "!grids list  -> Show GPS coordinates of Your grids \n" +
                                            "                ( helpful if U lost one ) \n" +
                                            "____________________________________________________________________ \n" +
                                            " \n" +
                                            "2. LINKS: \n" +
                                            " \n" +
                                            "> www.rebels-games.com < \n" +
                                            " \n" +
                                            "Website where U can find info about: \n" +
                                            "- Description of universe and galaxy map \n" +
                                            "- Instruction how to trade on neutral station \n" +
                                            "- Shop for Credits and SuperCredits \n" +
                                            "- Frequently Asked Questions ( FAQ ) \n" +
                                            " \n" +
                                            " \n" +
                                            "> https://discord.gg/KWmrpey < \n" +
                                            " \n" +
                                            "Chanel where U can speak with all REBELS-GAME crew and Players \n" +
                                            " \n" +
                                            " \n" +
                                            "> https://www.paypal.me/lostspace < \n" +
                                            " \n" +
                                            "Here U can donate to help us maintain the server ( 133 EURO / MONTH ) \n" +
                                            "Any amount is helpful, and U will gain Credits or SuperCredits! \n" +
                                            "____________________________________________________________________ \n" +
                                            " \n" +
                                            "3. How does a server jump work? \n" +
                                            " \n" +
                                            "REBELS-GAMES allows U to play on few synchronized servers: \n" +
                                            "Alpha, Beta, Gamma and Delta \n" +
                                            "To enable jumping sequence U have to sit in a COCKPIT or a FLIGHT SEAT \n" +
                                            "Then type in chat: \n" +
                                            " \n" +
                                            "!join 1 -> jump to Alpha  \n" +
                                            "!join 2 -> jump to Beta \n" +
                                            "!join 3 -> jump to Gamma  \n" +
                                            "!join 4 -> jump to Delta \n" +
                                            " \n" +
                                            "and wait 10 minutes for automatic relog on selected server. \n" +
                                            "Don't leave the cockpit ( flight seat ) or jump will be interrupted. After the jump, on a new server U HAVE TO chose: \n" +
                                            "!!! SPACE SUIT !!! \n" +
                                            "or You won't appear in Your cockpit ( flight seat ). \n" +
                                            "Your grid will spawn close to CENTER of the galaxy, where all the new ships begin. \n" +
                                            " \n" +
                                            "KNOWN ISSUES: \n" +
                                            "- Don't enter the target server before the jump ! \n" +
                                            "  ( It may cause Your grid to disappear after the jump, because of Your character recent log) \n" +
                                            "  * In that case wait for a longer time before the jump. \n" +
                                            "____________________________________________________________________ \n" +
                                            " \n" +
                                            "4. RULES IN REBELS-GAMES: \n" +
                                            " \n" +
                                            "- Each grid ( ship or station ) must have a BEACON ! \n" +
                                            "  ( cleaning of grids without Beacon is done 4 times a day on server restart) \n" +
                                            "- The name of the grid should start with your nickname. \n" +
                                            "  ( example: Player Ship or Player Station ) \n" +
                                            "- Construction of players not playing for 14 days will be removed. \n" +
                                            "- Behave decently and culturally in the game. \n" +
                                            "- Grids located more than 1500 km from center will be removed. \n" +
                                            "  ( during the server cleaning process when restart) \n" +
                                            "- On the first day of the month asteroids will change position. \n" +
                                            "____________________________________________________________________ \n" +
                                            " \n" +
                                            "5. MODDED BLOCKS ( more properties in game ) \n" +
                                            " \n" +
                                            "For more ore information enter -> /oreinfo \n" +
                                            " \n" +
                                            "- MK2-5    -> Multiply block properties, but also his mass and cost \n" +
                                            "              ( MK blocks requires PALLADIUM COMPONENTS ) \n" +
                                            "  MK2 x2 \n" +
                                            "  MK3 x4 \n" +
                                            "  MK4 x8 \n" +
                                            "  MK5 x16 \n" +
                                            " \n" +
                                            "- REBEL    -> New blocks not included in vanilla game \n" +
                                            "              ( Most REBEL blocks require SUPER COMPONENTS which U \n" +
                                            "              can buy at NEUTRAL STATION for Super Credits) \n" +
                                            "              Super Credits can be bought at the SHOP on www.rebels-games.com \n" +
                                            " \n" +
                                            "- ADVANCED -> Super blocks that are more powerful than MK blocks, which \n" +
                                            "              require unique ore from DEAD SUN in the ALPHA galaxy \n" +
                                            "              ( Be aware! DEAD SUN can easy burn Your ship! ) \n" +
                                            " \n" +
                                            "- CANNONS  -> REBEL weapons (chose one in game for more info ) \n" +
                                            "              ( All weapons have 800m range ) \n" +
                                            " \n" +
                                            "  Require dedicated ammo: \n" +
                                            "   20mm - fire: x4   speed:  700   damage:  300 \n" +
                                            "   25mm - fire: x4   speed:  300   damage:  370 explosion dmg \n" +
                                            "   55mm - fire: x1   speed: 1000   damage:  900 explosion dmg \n" +
                                            "  250mm - fire: x2   speed:  100   damage: 1700 \n" +
                                            "____________________________________________________________________ \n" +
                                            " \n" +
                                            "6. LIMITS OF BLOCKS ON SERVERS: \n" +
                                            " \n" +
                                            "10,000  - Per player \n" +
                                            " 5,000  - Per grid ( ship or station ) \n" +
                                            " \n" +
                                            "20 pcs - Drills per player \n" +
                                            "20 pcs - Welding per player \n" +
                                            "20 pcs - Grinders per player \n" +
                                            " 4 pcs - Refiners per player \n" +
                                            " 4 pcs - Assemblers per player  \n" +
                                            " 4 pcs - Arc furnaces per player \n" +
                                            " 2 pcs - Programmable blocks per player \n" +
                                            " 1 pc  - Projector per player \n" +
                                            "30 pcs - Interior Turrets per player \n" +
                                            "10 pcs - Gatling Turrets per player  \n" +
                                            " 8 pcs - Missile Turrets per player \n" +
                                            "10 pcs - Advanced Rebel Cannons per player";

                    MyAPIGateway.Utilities.ShowMissionScreen("About Rebels", "Server information", " ", helpMessage7, null, "I understand");

                    break;

                case "gps":
                    long playerID = MyAPIGateway.Session.Player.PlayerID;
                    double maxD = 1000000000.0;
                    List<VRage.Game.ModAPI.IMyGps> gps = MyAPIGateway.Session.GPS.GetGpsList(playerID);

                    // Check the existing list, if there is a GPS coord at zplus then niavely assume they have already run this command ;)
                    foreach (VRage.Game.ModAPI.IMyGps g in gps) {
                        if( g.Coords.X == 0.0 && g.Coords.Y == 0.0 && g.Coords.Z == maxD )
                        {
                            WriteMessage("Rebels-Games.com");
                            return;
                        }
                    }
                    VRage.Game.ModAPI.IMyGps origin = MyAPIGateway.Session.GPS.Create("Hoth", "", new VRageMath.Vector3D(277511.0, -228658.0, 829104.0),true);
                    VRage.Game.ModAPI.IMyGps zplus  = MyAPIGateway.Session.GPS.Create("Menus",   "", new VRageMath.Vector3D(-532607.0, -105911.0, -878071.0), true);
                    VRage.Game.ModAPI.IMyGps zminus = MyAPIGateway.Session.GPS.Create("Europe",     "", new VRageMath.Vector3D(-1218808.0, 702272.0, -269066.0), true);
                    VRage.Game.ModAPI.IMyGps qplus  = MyAPIGateway.Session.GPS.Create("Neutral Station",  "", new VRageMath.Vector3D(16993.0, 24281.0, -5837.0), true);

                    MyAPIGateway.Session.GPS.AddGps(playerID, origin);
                    MyAPIGateway.Session.GPS.AddGps(playerID, zplus);
                    MyAPIGateway.Session.GPS.AddGps(playerID, zminus);
                    MyAPIGateway.Session.GPS.AddGps(playerID, qplus);

                    break;
                case "range":
                case "r:":
                    if( words.Count == 2 )
                    {
                        if (words[1] == "off")
                        {
                            doRange = false;
                            filterGPS(false, true, false, false, "");
                        }
                        else {
                            decimal r;
                            if( decimal.TryParse(words[1],out r) )
                            {
                                doRange = true;
                                range = (double)r;
                            } else
                            {
                                MyAPIGateway.Utilities.ShowMessage("GPS", "Unknown range argument: " + words[1]);
                            }
                        }
                    }
                    break;
                

            }
        }

        // Filter out the appropriate GPS coords.
        void filterGPS( bool hideAll, bool showAll, bool hide, bool show, string tag)
        {
            long playerID = MyAPIGateway.Session.Player.PlayerID;
            List<VRage.Game.ModAPI.IMyGps> gps = MyAPIGateway.Session.GPS.GetGpsList(playerID);
            foreach (VRage.Game.ModAPI.IMyGps g in gps)
            {
                if (hideAll == true || (hide == true && (g.Name.Contains(tag) || g.Description.Contains(tag))))
                {
                    MyAPIGateway.Session.GPS.SetShowOnHud(playerID, g.GetHashCode(), false);
                }
                if (showAll == true || (show == true && (g.Name.Contains(tag) || g.Description.Contains(tag))))
                {
                    MyAPIGateway.Session.GPS.SetShowOnHud(playerID, g.GetHashCode(), true);
                }
            }
        }

        // Writes to SpaceEngineers/Storage
        void log( string text )
        {
            using (TextWriter writer = MyAPIGateway.Utilities.WriteFileInGlobalStorage("Log.txt"))
            {
                writer.WriteLine(text);
            }
        }

        #region post_script
    }
    }
#endregion post_script